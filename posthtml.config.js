module.exports = {
    plugins: {
        "posthtml-markdownit": {},
        "posthtml-noopener": {},
        "posthtml-expressions": {
            locals: {
                ROOT_URL: process.env.ROOT_URL || "https://readinglist.straightouttacrompton.co.uk",
            },
        },
    },
};
